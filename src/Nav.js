import React, { useEffect, useState } from 'react'
import "./Nav.css"

function Nav() {

    const [show, handleShow] = useState(false)

    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.scrollY > 100) {
                handleShow(true)
            } else handleShow(false)
        })
        return () => {
            /* With this we can remove the listener right before useEffect fires up
            again, so we dont have several listeners. */
            window.removeEventListener("scroll")
        }
    }, [])

    return (
        /* ${show && "nav__black"}` if show is true we attach the class nav__black */
        <div className={`nav ${show && "nav__black"}`}>
            <img
                className="nav__logo"
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Netflix_2015_logo.svg/800px-Netflix_2015_logo.svg.png"
                alt="Netflix Logo">
            </img>

            <img
                className="nav__avatar"
                src="https://upload.wikimedia.org/wikipedia/commons/0/0b/Netflix-avatar.png"
                alt="Netflix Avatar">
            </img>
        </div>
    )
}

export default Nav
