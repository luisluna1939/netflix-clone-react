import axios from "axios";

/**base url to make requests to the movie database */
const instance = axios.create({
    baseURL: "https://api.themoviedb.org/3"
})

//with default export we can rename the variable/compound when imported
export default instance;