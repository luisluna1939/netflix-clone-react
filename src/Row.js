import React, { useState, useEffect } from 'react'
import axios from './axios.js' //instance from axios.js renamed as axios
import Youtube from 'react-youtube'
import movieTrailer from 'movie-trailer'
import "./Row.css"

const base_url = "https://image.tmdb.org/t/p/original/" //Base URL for movie images

function Row({ title, fetchUrl, isLargeRow }) {
    //State to get movies
    const [movies, setMovies] = useState([])
    //state to set a trailerURL
    const [trailerUrl, setTrailerUrl] = useState("")

    //A snippet of code which runs based on a specific condition/variable
    useEffect(() => {
        //If [](empty), run once when the row loads, and dont run it again. If you have some variable [fetchUrl], it'll run every time that variable changes.
        async function fetchData() {
            const request = await axios.get(fetchUrl)
            // "https://api.themoviedb.org/3"
            setMovies(request.data.results)
            return request
        }
        fetchData()
    }, [fetchUrl])

    /* Options for embedded youtube player. Took from youtube documentacion */
    const opts = {
        height: "390",
        width: "100%",
        playerVars: {
            //https://developers.google.com/youtube/player_parameters?hl=es
            autoplay: 1
        }
    }

    /* When we click a poster a trailer from youtube appears */
    const handleClick = (movie) => {
        /* First we set on empty the trailerUrl so player video does not appear on page load  */
        if (trailerUrl) {
            setTrailerUrl('')
        } else {
            /* movieTrailer(movieName) is a function from movie-trailer package.
            This searchs for a trailer URL on youtube */
            movieTrailer(movie?.title || movie?.name || movie?.original_name)
                .then(url => {
                    /*https://www.youtube.com/watch?v=XtMThy8QKqU&t=9209s
                    Being this "v=XtMThy8QKqU&t=9209s" the videoId 
                    
                    To get that we use this next line
                    */
                    const urlParams = new URLSearchParams(new URL(url).search)
                    //We get the id that's after v=
                    setTrailerUrl(urlParams.get('v'))
                }).catch(error => console.log(error))
        }
    }

    //console.table shows arrays as a table, alternative to console.log
    //console.table(movies)

    return (
        <div className="row">
            <h2>{title}</h2>

            <div className="row__posters">
                {/* several row__poster */}

                {movies.map((movie) => {
                    return <img
                        key={movie.id} /*key is like a unique id for every item. The key is useful for rendering optimization, when a item changes, React only 
                    re-renders that only item */
                        onClick={() => handleClick(movie)}
                        className={`row__poster ${isLargeRow && "row__posterLarge"}`} /*isLargeRow && "row__posterLarge" if isLargeRow is true then adds the class
                        row__posterLarge */
                        src={`${base_url}${isLargeRow ? movie.poster_path : movie.backdrop_path}`}
                        alt={movie.name} />
                })}
            </div>
            {/*If trailerUrl is there then we show the player */}
            {trailerUrl && <Youtube videoId={trailerUrl} opts={opts} />}
        </div>
    )
}

export default Row
