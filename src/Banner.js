import React, { useEffect, useState } from 'react'
import axios from './axios'
import requests from './requests'
import "./Banner.css"

const base_url = "https://image.tmdb.org/t/p/original/" //Base URL for movie images

function Banner() {
    const [movie, setMovie] = useState([])

    useEffect(() => {
        async function fetchData() {
            const request = await axios.get(requests.fetchNetflixOriginals)
            setMovie(
                request.data.results[
                Math.floor(Math.random() * request.data.results.length - 1)
                ]
            ) /* Math.floor(Math.random() * request.data.results.length - 1) with this selects a random number from 0 to the end of the array results and
               use it to select a random item.*/
        }
        fetchData()
    }, [])

    console.log(movie)

    /*Function that shows three points "..." if description is too large */
    function truncate(str, n) {
        return str?.length > n ? str.substr(0, n - 1) + "..." : str
    }

    return (
        <header className="banner"
            style={{ //the "?" in movie?.backdrop_path checks if the variable is undefined then it handles the error
                backgroundSize: "cover",
                backgroundImage: `url(
                  ${base_url}${movie?.backdrop_path} 
            )`,
            }}
        >
            <div className="banner__contents">         {/** <<<Background image */}
                <h1 className="banner__title">
                    {movie?.title || movie?.name || movie?.original_name} {/* We have three variables cause items can come with either of this names for movie title */}
                </h1>
                <div className="banner__buttons">
                    <button className="banner__button">Play</button>
                    <button className="banner__button">My List</button>
                </div>

                <h1 class="banner__description">
                    {truncate(movie?.overview, 150)}
                </h1>
            </div>

            <div class="banner--fadeBottom"></div>
        </header>
    )
}


export default Banner
